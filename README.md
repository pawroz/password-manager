# Password Manager Website
- Django
- Python
- Docker
- Nginx
- SSL
- Html
- CSS

**See full documentation in files of repository -  Documentation_PMW.pdf**

Currently, it uses the master account password to encrypt the stored passwords **twice.** Even in the event of a large data breach in production, where all passwords are leaked, the passwords would still be **very** secure. 
