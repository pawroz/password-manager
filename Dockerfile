FROM python:3.8-alpine
LABEL maintainer='pablo'

ENV PYTHONUNBUFFERED 1
ENV PATH="/code/scripts:${PATH}"

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN pip install -r /requirements.txt
RUN apk del .tmp


RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN chmod +x /code/scripts/*
RUN mkdir -p /vol/pwd/media
RUN mkdir -p /vol/pwd/static
RUN adduser -D user
RUN chown -R user:user /vol
RUN chmod -R 755 /vol/pwd
USER user

ENTRYPOINT ["entrypoint.sh"]

RUN echo dupa

